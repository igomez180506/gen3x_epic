###_Removing file(s) from a branch and updating tree_

- git rm Git_wiki.md README.md : This removes removes the file from my cloned directory.

- git status: Checks for any changes recently to be commited or untracked files.

- git commit -m 'Deleting files from brach'

- git push origin RabbitMQ : Push the changes to Git to the particular branch.

------------------------------------------------------------
###_Adding a file from cloned repository to GitRepository hosted_

- git add Git_Wiki.md: Tell git to track a new file that is being added.

- git status: Check status of file and stage current status.

- git commit -m 'Git Wiki': This takes the staged snapshop and commits it to the project history.

-Note: Using -m indicates that a commit message follows.

- git push origin master: This pushes the update the the branch in Bitbucket on origin(BitBucket Server)
-------------------------------------------------------------
###_Working with Branches in Git_


###_Adding a directory or folder in Github

- Make sure it is not empty before adding it to repository.  Git does not stores empty folders.
- After folder is created and files are inside, follow same add/commit/push procedure as adding a file.
--------------------------------------------------------------

###_Creating and working with branch in GitHub_

- Command below creates a branch on local machine and switch to that branch

git checkout -b BranchCreated

- Changing to working branch

git checkout BranchSwitched

- Push the branch to github

git push orign NameofBranch

- Note: When there is a need to commit something in your branch, be sure to be in your branch
Add -u parameter to set upstream.

- To list all branches, it is possible to use: git branch -a

- git branch.  Shows all branches created.

###_Deleting a branch_

- Deletes a branch in local filesystem:
git branch -d [name_of_your_new_branch]
- Delets the branch on github: 
git push origin :[name_of_your_new_branch]
- To force the deletion of local branch on your filesystem :
$ git branch -D [name_of_your_new_branch]

###_Git Pull Request_

- git pull does a git fetch followed by a git merge to update local repository with the remote repo.

Example: git pull origin master(branch)

- Enter the  git pull --all  command to pull all the changes from Bitbucket.

###_Fetching
- git fetch
Only fetch the specified branch

- git fetch --all
 fetches all registered remotes and their branches

- git fetch
- git fetch will download all the recent changes, but it will not put it in your current checked out code (working area).

- git fetch --dry-run
The --dry-run option will perform a demo run of the command.

-git checkout origin/master -- path/to/file
//git checkout <local repo name(default is origin)>/<branch name> -- path/to/file will checkout the particular file from the the downloaded changes (origin/master).

git fetch --all
git checkout origin/master -- <your_file_path>
git add <your_file_path>
git commit -m "<your_file_name> updated"
This is assuming you are pulling the file from origin/master.

###Note:
- In the simplest terms, git pull does a git fetch followed by a git merge . You can do a git fetch at any time to update your remote-tracking branches under refs/remotes/<remote>/ . This operation never changes any of your own local branches under refs/heads , 
and is safe to do without changing your working copy

- A git pull is what you would do to bring a local branch up-to-date with its remote version, while also updating your other remote-tracking branches
- git fetch at any time to update your remote-tracking branches under refs/remotes/<remote>/.

###_References:
https://github.com/Kunena/Kunena-Forum/wiki/Create-a-new-branch-with-git-and-manage-branches
https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging
https://yangsu.github.io/pull-request-tutorial/
