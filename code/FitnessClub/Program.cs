﻿using System;
//Inheriance.
//Polymorphism: A program ability to use the correct method for an object based on its run-time type.
namespace FitnessClub
{
    class Member{
        //class fields
        protected int annualFee;
        private string name;
        private int memberID;
        private int memberSince;

  
    //Method  to display the values of the 4 fields
    public override string ToString(){
        return "\nName:" + name + "\nMember ID: "
         + memberID + "\nMember Since: "
         + memberSince 
         + "\nTotal Annual Fee: " 
         + annualFee;
    }
    //class constructors
    public Member(){
        Console.WriteLine("Parent Constructor with no parameters");
    }

    public Member(string pName, int pMemberID, int pMemberSince)
    {
        Console.WriteLine("Parent Constructor with 3 parameters");
        name=pName;
        memberID=pMemberID;
        memberSince=pMemberSince;
        Console.WriteLine("Call from MemberConstructor itself: {0}:", ToString());
    }
    //virtual keyword tells compiler that this method may be overriden in derived class.
    //When this keyword is found, compiler looks for the same method in derived class and execute this method instead.
    public virtual void CalculateAnnualFee(){
        annualFee=0;
    }
 }
 //child class NormalMember(When creating a child object, the parent class constructor is called first.)
 class NormalMember : Member{
     //Constructor declaration without parameters.  Hence object created will trigger constructor from superclass without parameters
     //first then the constuctor in the derived class with no parameters.
     public NormalMember(){
         Console.WriteLine("Child constructor with no parameter");
     }
     //2nd way to do it.  Using :base(), this call constructors with parameters in the super class.  Hence values are passed to the parent
     //constructor and hence assigned to the respective fields in the base class.  In this case, fixed values are passed as arguments
     //to the base constructor.
    /* public NormalMember(string remarks): base("Jamie", 1, 2015){
         Console.WriteLine("Remarks = {0}", remarks);
     }*/

     //Better way: Passing the arguments through the child constructor
     public NormalMember(string remarks, string name, int memberID, int memberSince):base(name, memberID, memberSince){
         Console.WriteLine("Child constructor with 4 parameters");
         Console.WriteLine("Remarks={0}", remarks);
     }

     //Child class method which overrides method in the base class.
     public override void CalculateAnnualFee(){
         annualFee= 100+12*30;
     }


 }
 class VIPMember : Member{
     public VIPMember(string name, int memberID, int memberSince) : base (name, memberID, memberSince){
         Console.WriteLine("Child Constructor with 3 parameters");
     }
     public override void CalculateAnnualFee(){
         annualFee=1200;
     }
 }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main Method");
           /* NormalMember mem1 = new NormalMember("Special Rate", "James", 1, 2010);
            VIPMember    mem2 = new VIPMember("Andy", 2, 2011);
            mem1.CalculateAnnualFee();
            mem2.CalculateAnnualFee();
            Console.WriteLine(mem1.ToString());
            Console.WriteLine(mem2.ToString());*/
            //--- Polymorphism steps----
            Member[] clubMembers = new Member[5];
           
            clubMembers[0] = new NormalMember("Special Rate", "James", 1, 2010);
            clubMembers[1] = new NormalMember("Normal Rate", "Andy", 2, 2011);
            clubMembers[2] = new NormalMember("Normal Rate", "Bill", 3, 2011);
            clubMembers[3] = new VIPMember("Carol", 4, 2012);
          if(clubMembers[3].GetType() == typeof(NormalMember)){
                 Console.WriteLine("Yes, this is a test!");
            }
            else
            {
                    Console.WriteLine("NO, it is not of same type");
            }
                 clubMembers[4] = new VIPMember("Evelyn", 5, 2012);
            foreach(Member m in clubMembers){
                m.CalculateAnnualFee();
                Console.WriteLine(m.ToString());
                
            }

        }
    }
}
