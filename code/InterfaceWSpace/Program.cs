﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceWSpace
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
    //Standard: Name of interface shall start with letter I.
    //Only properties and methods with no body in the interface.
    interface IShape{
        int MyNumber{
            get;
            set;
        }
        void InterfaceMethod();
    }
    class ClassA : IShape{
        private int myNumber;
        public int MyNumber{
            get{
                return myNumber;
            }
            set{
                if(value <0)
                    myNumber = 0;
                else    
                    myNumber - value;
            }
                
            }
            public void InterfaceMethod(){
                Console.WriteLine("The number is {0}:", myNumber);
            }
        }
    }
