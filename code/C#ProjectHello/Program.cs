﻿//Directives: Tell compiler to use certain namespaces as part of the program
//namespace: Group of related code elements such as: classes, interfaces, enums,
//structs, and so on.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HelloWorld
{
    /*Simple first program to display the words HelloWorld
    A new template was created using dotnet new console --name (directoryname)
    Initial template information is included in Program.cs with project structure
    under directory's name.  Program.cs is where the main directory is found, but does not have
    to be called Program.cs.  It could be changed to any name, but very project should have this
    entry point.*/
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //Method use to force program to wait for a key press before closing
            //window or process.
            Console.ReadLine();
        }
    }
}//END of namespace HelloWorld.
