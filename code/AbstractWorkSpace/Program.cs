﻿using System;

namespace AbstractWorkSpace
{
    abstract class MyAbstractClass{
    private string message="Hello C#";
    public void PrintMessage(){
        Console.WriteLine(message);
         }
    public abstract int add();      
   
    public abstract void PrintMessageAbstract();
        
    }
    class ClassA : MyAbstractClass{
        public override void PrintMessageAbstract(){
            Console.WriteLine("C# is fun!" +  "Implemented in derived class Class A");
        }
      public override int add(){
        int total=0;

        return total;
    }        
        
    }
    class Program
    {
        static void Main(string[] args)
        {
            ClassA a= new ClassA();
            ClassA b = new ClassA();
            a.PrintMessage();
            a.PrintMessageAbstract();
            
            Console.WriteLine(b.add());
            Console.Read();

        }
    }


}
