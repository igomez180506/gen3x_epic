﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HelloWorldP2
{
    class Program
    {
        static string userName=" ";
        static int userAge=0;
        static int currentYear=0;
        static string defname="people";
        
        static void Main(string[] args)
        {
            Boolean state=true;
            Console.Write("Please enter your name: ");
            userName=Console.ReadLine();
            Console.Write("Please enter your age: ");
            userAge = Convert.ToInt32(Console.ReadLine());
            Console.Write("Please enter the current year: ");
            currentYear=Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Hello {0} My name is {1} and I am {2} years old. I was born in {3}.", defname, userName, userAge, currentYear - userAge);
            Console.Write("Are you Single or Marriage?");
            state=Convert.ToBoolean(Console.ReadLine());
            if (state!=false){
                Console.WriteLine("Congratulations, you are single!");
            }
            else{
                Console.WriteLine("I guess you are marriage");
            }     
        }
        
    }
}
