@startuml
title Diagrama de Sequencia de la Aplicacion Mobil
AplicacionMobil->BaseDeDatos:Manda informacion de geolocalizador
note right of AplicacionMobil
Tiempo de salida
Tiempo de llegada
Locacion en tiempo real
Duracion en cada punto
Tiempo en llegar a cada punto
end note
AplicacionMobil->BaseDeDatos:Manda informacion del inventario
note right of AplicacionMobil
Verification del inventario del almacen
Verificacacion de precios de los productos en la tienda.
end note
AplicacionMobil->BaseDeDatos:Accesso
note left of AplicacionMobil
Recive credentiales de accesso
Processa las credenciales y las manda
end note
DesktopUI->BaseDeDatos: Administrador interactua con el systema
note right of DesktopUI
Entra informacion de los usuarios
crea nuevos usuarios y su perfil
Recive credentiales del administrator
Muestra informacion del inventario
Muestra informacion mandada atravez del geolocalizador en tiempo real e historial de cada usuario por separado y en conjunto.
Imprime reporte del geolocalizador
Imprime en forma de pdf
imprime reporte del inventario
end note
BaseDeDatos->AplicacionMobil:Procesa Informacion y la hace dispobible
note left of BaseDeDatos
Guarda informacion del geolocalizador y la hace disponible al administrador
Verifica credentiales y provee accesso basado en el perfil associado y derechos del usuario.
Provee accesso al inventario basado en el perfil asociado para verificacion de cantidad
Provee accesso al inventario basado en el perfil asociado para verificacion de precios
Guarda informacion que entra al inventario y lo actualiza
end note
BaseDeDatos->DesktopUI:Procesa informacion y la hace disponible
note right of BaseDeDatos
Ejecuta informacion de los usuarios
Hace informacion del usuario disponible para busquedas
Actualiza inventario y la hace disponible
Actualiza informacion del geolocalizador y la hace disponible
end note
@enduml