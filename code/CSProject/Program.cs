﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace CSProject
{
    class Staff{
        //Class fields
        private float hourlyRate;
        private int hWorked;

        //Auto Implemented Properties
        public float TotalPay{get;protected set;}
        public float BasicPay{get;private set;}
        public string NameOfStaff{get;private set;}

        //Non-Auto Implemented Properties--when condition is needed in set.
        //Backing fields.
        public int HoursWorked{
            get{

                return hWorked;
            }
            set{
                if (value > 0) //value is a keyword when use inside setter.
                     hWorked=value;
                else
                    hWorked=0;
            }

        }
        //Constructor
        public Staff(string name, float rate){
            NameOfStaff=name;
            hourlyRate=rate;
        }
        //Class Methods
        public virtual void CalculatePay(){
            Console.WriteLine("Calculating Pay...");

            BasicPay=hWorked*hourlyRate;
            TotalPay=BasicPay;
        }
        public override string ToString(){
            Console.WriteLine("Informatiom coming from ToString....");
            return "Hourly Rate= " + hourlyRate +", TotalPay= "+TotalPay+", BasicPay="+BasicPay+", NameOfStaff="+NameOfStaff+", hworked=" + hWorked;
        }
    }
        class Manager:Staff{
            //constant field
            private const float  managerHourlyRate=50;

            //Auto-Implemented property
            public int Allowance{get;private set;}

            //Constructor
            public Manager(string name):base(name, managerHourlyRate){

            }
            //Method to override CalculatePay()
            public override void CalculatePay(){
                //Calling virtual method in base class
               base.CalculatePay();
               Allowance=1000;

               if (HoursWorked>160){
                   TotalPay=BasicPay + Allowance;
               }

            }
            public override string ToString(){
                Console.WriteLine("Informatiom coming from ToString.... Manager class..");
            return "managerHourlyRate= " + managerHourlyRate +", Allowance= "+Allowance+", TotalPay="+TotalPay+", NameOfStaff="+ NameOfStaff;
            }

        }
        class Admin : Staff{
            //class fields
            private const float overtimeRate=15;
            private const float adminHourlyRate=30;
            //Auto Implemented property
           public float Overtime{get;private set;}
          //constructor
          public Admin(string name):base(name, adminHourlyRate){}
          public override void CalculatePay(){
              base.CalculatePay();

              if (HoursWorked > 160){
                  Overtime=overtimeRate * (HoursWorked -160);
              }
          }
           public override string ToString(){
                Console.WriteLine("Informatiom coming from ToString.... Admin class..");
            return "adminHourlyRate= " + adminHourlyRate +", overtimeRate= "+overtimeRate+", Overtime="+Overtime+", NameOfStaff="+ NameOfStaff;
            }
        }

        //File reader class
        class FileReader{
            //Method returns a list of staf objects.
            //list:dynamic size(can grow). It is an object that holds items or variables value in specific order. it can be removed or added.
            //A possible way to ouptut its value to console is list<T>.ForEach(Console.WriteLine);
            public List<Staff> ReadFile(){
                //local variables.
                List<Staff> myStaff = new List<Staff>();
                string[] result = new string [2];
                string path = "home//dev//sourcecontrol//givan//gen3x_epic//staff.txt";
                string [] separator ={", " };

                if(File.Exists(path)){
                    using(StreamReader sr = new StreamReader(path)){
                        while(!sr.EndOfStream){

                          result=sr.ReadLine().Split(separator,StringSplitOptions.RemoveEmptyEntries);
                          if (result[1]=="Manager")
                                 myStaff.Add(new Manager(result[0]));
                          else if (result[1] == "Admin")
                                 myStaff.Add(new Admin(result[0]));
                        }
                        sr.Close();
                    }
                }else{
                    Console.WriteLine("Sorry, the file does not exist!");
                }

                return myStaff;
            }
        }
    class PaySlip{
        //Fields
        private int month;
        private int year;
        //enum:Special datatype to create constants. By default enum declared inside a class is private.
        enum MonthOftheYear{
            JAN=1, FEB=2, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC 
        }
        public PaySlip(int payMonth, int payYear){
            this.month = payMonth;
            this.year  = payYear;
        }
        public void GeneratePaySlip(List<Staff>myStaff){
            string path;
            foreach(Staff f in myStaff){
                path=f.NameOfStaff+".txt";
                using (StreamWriter sw = new StreamWriter(path)){
                    sw.WriteLine("PAYSLIP FOR {0} {1}", (MonthOftheYear)month, year);//month is int, hence cast into enum to access enum value. Ex: (EnunName)int(12)=DEC
                    sw.WriteLine ("===============================================");
                    sw.WriteLine ("Name of Staff: {0}", f.NameOfStaff);
                    sw.WriteLine ("Hours Worked : {0}", f.HoursWorked);
                    sw.WriteLine ("");
                    //Using {:C}, adds $ symbol in front of the number.
                    //Displays the number with 2 decimal places
                    //Separates every thousand with comma.
                    sw.WriteLine ("Basic Pay: {0:C}", f.BasicPay);
                    //GetType(): Returns the runtime type of an Object.
                    //typeof(): Takes the name of the data type and returns the type of that name and then can be used
                    //to compare with the result of GetType().
                    if (f.GetType () == typeof (Manager))
                    sw.WriteLine("Allowance: {0:C}", ((Manager)f).Allowance);//Casting f into a Manager object to access Allowance proprty.
                    else if (f.GetType () == typeof(Admin))
                        sw.WriteLine("Overtime : {0:C}", ((Admin)f).Overtime);
                    sw.WriteLine("");
                    sw.WriteLine("==================================================");
                    sw.WriteLine("Total pay: {0:C}", f.TotalPay);
                    sw.WriteLine("==================================================");
                    sw.Close();
                }
            }

        }
        public void GenerateSummary(List<Staff>myStaff){
            var result=from f in myStaff where f.HoursWorked < 10 orderby f.NameOfStaff ascending select new {f.NameOfStaff, f.HoursWorked};
            string path="summary.txt";
            using (StreamWriter sw = new StreamWriter(path)){
                sw.WriteLine("Staff with less than 10 hours working");
                sw.WriteLine("");
                foreach(var f in result)
                    sw.WriteLine("Name of Staff: {0}, Hours Worked: {1}", f.NameOfStaff, f.HoursWorked);

                    sw.Close();
                }
            }
            public override string ToString(){
                return "month= " + month + "year=" + year;
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Payroll project from Learn C# in one day!...Main method");
            List<Staff> myStaff = new List<Staff>();
            FileReader fr = new FileReader();
            int month=0;
            int year=0;
            

            while(year==0){
                Console.Write("\nPlease enter the year: ");
             
                try{
                    //code to convert input to an integer
                    year=Convert.ToInt32(Console.ReadLine());

                }
                catch(Exception e){ 
                        //code to handle the exception
                        Console.WriteLine("Sorry, please try again {0}", e.Message);
                }
            }
        while (month==0){
            Console.Write("\nPlease enter month: ");
            try{
                  month=Convert.ToInt32(Console.ReadLine());
                if(month< 1 || month >12){

                  Console.WriteLine("Sorry, invalid input. It must be between 1 as 12");
                  month=0;
                 }

            }
            catch(Exception e){
                Console.WriteLine("Input is invalide, pleae try again: {0}", e.Message);
            }
  
        }
            myStaff = fr.ReadFile();
            for(int i=0; i<myStaff.Count; i++)
            {
                try{
                    Console.Write("\nEnter hours worked for {0}: ",
                    myStaff[i].NameOfStaff); 
                    myStaff[i].HoursWorked=Convert.ToInt32(Console.ReadLine()); 
                    myStaff[i].CalculatePay();

                    Console.WriteLine(myStaff[i].ToString());
               
                }catch(Exception e){
                    Console.WriteLine(e.Message);i--;
                }
            }
           PaySlip ps = new PaySlip(month, year); 
           ps.GeneratePaySlip(myStaff);
           ps.GenerateSummary(myStaff);

           Console.Read();//Prevent console from closing immediately.
        }
    }

