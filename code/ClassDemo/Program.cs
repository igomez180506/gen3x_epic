﻿using System;

namespace ClassDemo
{
    class Staff{
        //private fields of the class.
        private string nameOfStaff;
        private const int hourlyRate=30;
        private int hWorked;
        public static string greetings="Hello...";
        //static keyword:  Allow classes, members to be accessed without the need to create any objects using 
        //class name.
        //constructor: Construct the object created.  It is the first method that is called when an object
        //is created.  Commonly use to initialize the field of the class.
        //It is possible to have more than one constructor as long as the signature is different.
        //Static classes:  Can only contain static members.  Example Console class is static.  Thereore there is 
        //no need to create objects and we simply use Console(classname).WriteLine(Method)
        //Constructor#1
        public Staff(string name){
            nameOfStaff=name;
            Console.WriteLine("\n" + nameOfStaff);
            Console.WriteLine("-------------------------------------");
        }
        public Staff(string firstName, string lastName){
            nameOfStaff = firstName + "" + lastName;
            Console.WriteLine("\n" + nameOfStaff);
            Console.WriteLine("-------------------------------------------");
        }
            /*--When there is additional logic, this is the getter/setter format propery use*/
        public int HoursWorked{
            get{
                return hWorked;
            }
            set{
                if(value>0)
                    hWorked=value;
                else
                    hWorked=0;
            }
        }
        //*END of getter/setter property with additional logic.
        //Auto Implemented property.
        //property:  Allow access to private fields from outside classes.
        //In this case, it is set as public so outside class can access and modify private fields.
        //if setter is declared as private, outside classes can only read private fields.
        //In this case, property is read-only outside the class.  Its value can only be set within its
        //own class.
        //Style use below is called:AutoImplemented Property.  In case there is no additional logic required.(shorthand)
        //By default getter and setter have same public access level  as the property itself.
        //getter will return to the method that's using its field.
        //--No additonal logic
       // public int HoursWorked{get; set; }
    

        public void PrintMessage(){
            Console.WriteLine("Calculating Pay...");
        }
        public int CalculatePay(){
            //Accessing method from same class. (.) operator is not need.
            PrintMessage();
       
            int staffPay;
         staffPay = hWorked * hourlyRate;
            if (hWorked > 0)

                return staffPay;
            else
              return 0;
        }
        //Overloading method: method with same name, but different in signature.
        public int CalculatePay(int bonus, int allowance)
        {
            PrintMessage();
            if(hWorked>0)
               return hWorked * hourlyRate + bonus + allowance;
            else
               return 0;
        } 
        //ToString() override method.  Overriding simply means writing our own version of the method.
        //All classes comes with a pre-defined ToString() method. Typically this method is written to display
        //the values of the fields and properties of the class.
        //override keyword indicates that this method overrides the default method.
        public override string ToString(){

            return "Name of Staff =" + nameOfStaff + ", hourlyRate=" + hourlyRate + ", hWorked=" + hWorked;

        }      
    }
    class Program
    {
        static void Main(string[] args)
        {
            int pay;
           //Object instantiation
           Console.Write(Staff.greetings);
           Staff staff1 = new Staff("Peter");
           Staff staff2 = new Staff("Jane", "Lee");
           Staff staff3 = new Staff("Carol");

           //Uses the instance to access the properties{get;set;}, set and return a value
           //.(dot)operator is used to access a field, property or method from another class.
           //in this case it is being accessed from class Program.
           //If it is a member of the same class, within the same class, (.) is not needed.
           //It sets the value of the private field and return it to the method that is using it with the getter.
           staff1.HoursWorked = 160;
           pay=staff1.CalculatePay(1000,400);
           Console.WriteLine("Pay={0}",pay);  
            
           staff2.HoursWorked =160;
           pay=staff2.CalculatePay();
           Console.WriteLine("Test:{0}",staff2.ToString());
           Console.WriteLine("Pay={0}", pay); 

           staff3.HoursWorked = -10;
           pay = staff3.CalculatePay();
           Console.WriteLine("Pay = {0}", pay);          
        }
    }
}
